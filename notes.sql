=====================
SQL CRUD Operations
=====================

1. (Create) Adding a record.
    Terminal
        Adding a record:
        Syntax
            INSERT INTO table_name (column_name) VALUES (value1);

2. (Read)Show all records.
    Terminal

        Displaying/retrieving records:
        Syntax
            SELECT column_name FROM table_name;

3. (Create) Adding a record with multiple columns.
    Terminal

        Adding a record with multiple columns:
        Syntax
            - INSERT INTO table_name (column_name, column_name) VALUES (value1, value2);

            --Note: Values must match the column.

5. (Create) Adding multiple records.
    Terminal

        Adding multiple records:
        Syntax
            INSERT INTO table_name (column_name, column_name) VALUES (value1, value2), (value3, value4);


6. Show records with selected columns.
    Terminal

       Retrieving records with selected columns:
        Syntax
            SELECT (column_name1, column_name2) FROM table_name;

        SELECT song_name, genre FROM songs;

7. (Read) Show records that meet a certain condition.
    Terminal

        Retrieving records with certain condtions:
        Syntax
            SELECT column_name FROM table_name WHERE condition;


8. (Read) Show records with multiple conditions.
    Terminal

        Displaying/retrieving records with multiple conditions:
        Syntax
            AND CLAUSE
                SELECT column_name FROM table_name WHERE condition1 AND condition2;
            OR CLAUSE
                SELECT column_name FROM table_name WHERE condition1 OR condition2;

9. (Update) Updating records.
    Terminal

        Add a record to update:
        INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Megalomaniac", 410, "Classical", 2);

        Updating records:
        Syntax
            UPDATE table_name SET column_name = value WHERE condition;

            UPDATE table_name SET column_name = value, column_name2 = value2 WHERE condition;

            --Note: When updating and deleting, add a where clause or else you may update or delete all items in a table.

10. (Delete) Deleting records.
    Terminal
        Deleting records:
        Syntax
            DELETE FROM table_name WHERE condition;
